﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreBinaire_Library
{
    // Exercice 1
    public class ArbreBinaire<TypeElement>
    {
        public NoeudArbreBinaire<TypeElement> Racine { get; set; }
        
        //Exercice 3
        public int Hauteur
        {
            get
            {
                return GetHauteur(Racine);
            }
        }
        private int GetHauteur(NoeudArbreBinaire<TypeElement> p_noeud)
        {
            if (p_noeud == null)
            {
                return 0;
            }

            return 1 + Math.Max(GetHauteur(p_noeud.Gauche), GetHauteur(p_noeud.Droit));
        }

        //Exercice 4
        public void ParcoursPrefixe(NoeudArbreBinaire<TypeElement> p_noeud, Action<TypeElement> p_traitement)
        {
            if(p_noeud != null)
            {
                p_traitement(p_noeud.Valeur);
                ParcoursPrefixe(p_noeud.Gauche, p_traitement);
                ParcoursPrefixe(p_noeud.Droit, p_traitement);
            }
        }
        public void ParcoursInfixe(NoeudArbreBinaire<TypeElement> p_noeud, Action<TypeElement> p_traitement)
        {
            if (p_noeud != null)
            {
                ParcoursInfixe(p_noeud.Gauche, p_traitement);
                p_traitement(p_noeud.Valeur);
                ParcoursInfixe(p_noeud.Droit, p_traitement);
            }
        }
        public void ParcoursPostfixe(NoeudArbreBinaire<TypeElement> p_noeud, Action<TypeElement> p_traitement)
        {
            if (p_noeud != null)
            {
                ParcoursPostfixe(p_noeud.Gauche, p_traitement);
                ParcoursPostfixe(p_noeud.Droit, p_traitement);
                p_traitement(p_noeud.Valeur);
            }
        }
    }
}
