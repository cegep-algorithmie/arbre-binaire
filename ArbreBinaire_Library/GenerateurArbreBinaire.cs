﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreBinaire_Library
{
    public class GenerateurArbreBinaire
    {
        //Exercice 2
        public static ArbreBinaire<int> ExempleArbre1()
        {
            return new ArbreBinaire<int>()
            {
                Racine = new NoeudArbreBinaire<int>()
                {
                    Gauche = new NoeudArbreBinaire<int>()
                    {
                        Gauche = new NoeudArbreBinaire<int>()
                        {
                            Gauche = null,
                            Droit = null,
                            Valeur = 1
                        },
                        Droit = new NoeudArbreBinaire<int>()
                        {
                            Gauche = null,
                            Droit = null,
                            Valeur = 3
                        },
                        Valeur = 2
                    },
                    Droit = new NoeudArbreBinaire<int>()
                    {
                        Gauche = null,
                        Droit = new NoeudArbreBinaire<int>()
                        {
                            Gauche = null,
                            Droit = null,
                            Valeur = 6
                        },
                        Valeur = 5
                    },
                    Valeur = 4
                }
            };
        }
        //Exercice 5
        public static ArbreBinaire<int> ExempleArbre2()
        {
            return new ArbreBinaire<int>()
            {
                Racine = new NoeudArbreBinaire<int>()
                {
                    Gauche = new NoeudArbreBinaire<int>()
                    {
                        Gauche = null,
                        Droit = null,
                        Valeur = -4
                    },
                    Droit = new NoeudArbreBinaire<int>()
                    {
                        Gauche = new NoeudArbreBinaire<int>()
                        {
                            Gauche = new NoeudArbreBinaire<int>()
                            {
                                Gauche = null,
                                Droit = null,
                                Valeur = 18
                            },
                            Droit = new NoeudArbreBinaire<int>()
                            {
                                Gauche = null,
                                Droit = null,
                                Valeur = 35
                            },
                            Valeur = 24
                        },
                        Droit = new NoeudArbreBinaire<int>()
                        {
                            Gauche = new NoeudArbreBinaire<int>()
                            {
                                Gauche = null,
                                Droit = null,
                                Valeur = 40
                            },
                            Droit = new NoeudArbreBinaire<int>()
                            {
                                Gauche = null,
                                Droit = null,
                                Valeur = 56
                            },
                            Valeur = 42
                        },
                        Valeur = 39
                    },
                    Valeur = 13
                }
            };
        }
    }
}
