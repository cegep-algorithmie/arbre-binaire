﻿using System;

namespace ArbreBinaire_Library
{
    // Exercice 1
    public class NoeudArbreBinaire<TypeElement>
    {
        public NoeudArbreBinaire<TypeElement> Gauche { get; set; }
        public NoeudArbreBinaire<TypeElement> Droit { get; set; }
        public TypeElement Valeur { get; set; }
    }
}
