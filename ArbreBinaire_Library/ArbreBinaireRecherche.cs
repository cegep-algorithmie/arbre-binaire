﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreBinaire_Library
{
    //Exercice 5
    public class ArbreBinaireRecherche<TypeElement> where TypeElement : IComparable<TypeElement>
    {
        public NoeudArbreBinaire<TypeElement> Racine { get; set; }
        public TypeElement Minimum()
        {
            NoeudArbreBinaire<TypeElement> noeud = this.Racine;

            while (noeud.Gauche != null)
            {
                noeud = noeud.Gauche;
            }

            return noeud.Valeur;
        }
        public TypeElement Maximum()
        {
            NoeudArbreBinaire<TypeElement> noeud = this.Racine;

            while (noeud.Droit != null)
            {
                noeud = noeud.Droit;
            }

            return noeud.Valeur;
        }
        /* Quelles sont les complexités de ces deux méthodes dans le meilleur des cas?
         * 
         * Les complexités de ces deux méthodes sont O(1) dans le meilleur des cas, soit il n'y a que la racine.
         * 
         * Quel type de parcours permet d’afficher les valeurs dans l’ordre croissant ?
         * 
         * Le parcours infixe permet d'afficher les valeurs d'un arbre binaire de recherche en ordre croissant. */

        //Exercice 6
        public void InsererValeur(TypeElement p_valeur)
        {
            this.InsererValeur_rec(this.Racine, p_valeur);
        }
        private void InsererValeur_rec(NoeudArbreBinaire<TypeElement> p_noeud, TypeElement p_valeur)
        {
            if(p_valeur.CompareTo(p_noeud.Valeur) < 0)
            {
                if(p_noeud.Gauche == null)
                {
                    p_noeud.Gauche = new NoeudArbreBinaire<TypeElement>()
                    {
                        Gauche = null,
                        Droit = null,
                        Valeur = p_valeur
                    };
                }
                else
                {
                    InsererValeur_rec(p_noeud.Gauche, p_valeur);
                }
            }
            else if(p_valeur.CompareTo(p_noeud.Valeur) > 0)
            {
                if (p_noeud.Droit == null)
                {
                    p_noeud.Droit = new NoeudArbreBinaire<TypeElement>()
                    {
                        Gauche = null,
                        Droit = null,
                        Valeur = p_valeur
                    };
                }
                else
                {
                    InsererValeur_rec(p_noeud.Droit, p_valeur);
                }
            }
            else
            {
                NoeudArbreBinaire<TypeElement> noeud = new NoeudArbreBinaire<TypeElement>()
                {
                    Gauche = p_noeud.Gauche,
                    Droit = null,
                    Valeur = p_valeur
                };
                p_noeud.Gauche = noeud;
            }
        }
        /* Déduisez :
         * 
         *  - Quelle est la hauteur idéale d’un arbre binaire de recherche ?
         *
         *  La hauteur idéale serait une hauteur Log2(N) + 1 pour un avoir un arbre bien équilibré.
         *
         *  - Quelle est la hauteur dans le pire des cas de l’arbre binaire de recherche ?
         *  
         *  La hauteur dans le pire des cas serait toutes les valeurs du même coté soit d'une hauteur N (hauteur équivalente au nombre de données). */
        public TypeElement RechercherValeur(TypeElement p_valeur)
        {
            return RechercherValeur_rec(this.Racine, p_valeur);
        }
        private TypeElement RechercherValeur_rec(NoeudArbreBinaire<TypeElement> p_noeud, TypeElement p_valeur)
        {
            TypeElement valeur = default;

            if (p_valeur.CompareTo(p_noeud.Valeur) < 0)
            {
                if (p_noeud.Gauche != null)
                {
                    RechercherValeur_rec(p_noeud.Gauche, p_valeur);
                }
            }
            else if (p_valeur.CompareTo(p_noeud.Valeur) > 0)
            {
                if (p_noeud.Droit != null)
                {
                    RechercherValeur_rec(p_noeud.Droit, p_valeur);
                }
            }
            else
            {
                valeur = p_valeur;
            }

            return valeur;
        }
    }
}
