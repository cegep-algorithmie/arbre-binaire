﻿using ArbreBinaire_Library;
using System;

namespace ArbreBinaire_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Exercice 4
            ArbreBinaire<int> arbreBinaire = GenerateurArbreBinaire.ExempleArbre1();
            Action<int> afficher = valeur => Console.WriteLine(valeur);

            Console.WriteLine("Parcours Prefixe");
            arbreBinaire.ParcoursPrefixe(arbreBinaire.Racine, afficher);
            Console.WriteLine("Parcours Postfixe");
            arbreBinaire.ParcoursPostfixe(arbreBinaire.Racine, afficher);
            Console.WriteLine("Parcours Infixe");
            arbreBinaire.ParcoursInfixe(arbreBinaire.Racine, afficher);
        }
    }
}
